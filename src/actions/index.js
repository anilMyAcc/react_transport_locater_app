export const Actions = {
    login: () => {
        return {
            type: 'SIGN_IN',
            text: 'Login in to application ..'
        }
    },
    logout: () => {
        return {
            type: 'SIGN_OUT',
            text: 'Logout from application ..'
        }
    }
}