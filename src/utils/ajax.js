export const ajaxCall = (
    urlPath,
    reqObj,
    successCallback = (res="data") => {
        console.log({response: res})
    },
    errorCallback = (err="error") => {
        console.log({response: err})
    }) => {
        fetch(urlPath, reqObj)
        .then(result => result.json())
        .then(result => {
                successCallback(result)
            }
        )
        .catch((error) => {
            errorCallback(error)
        })
}