import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Actions } from '../../actions';
import { useSelector, useDispatch } from 'react-redux';

import './styles.css';

export const Header = () => {
    const loginStatus = useSelector(state => state.loginStatus);
    const dispatch = useDispatch()
    return (
        <nav className="navbar shadow-1" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <span className="navbar-item"><Link className="nav_header" to="/">Locater</Link></span>
            </div>
    
            <div id="navbarBasicExample" className="navbar-menu">
                
            </div>
            <div className="navbar-end">
                <div className="navbar-item">
                    <div className="buttons">
                        {loginStatus ?
                            <button
                                className="button"
                                onClick={() => {
                                    dispatch(Actions.logout())
                                }}>
                                    Logout
                                </button>
                            : null
                        }
                    </div>
                </div>
            </div>
        </nav>
    )
}

// export default Header;