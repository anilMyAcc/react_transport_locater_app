import React, { useState } from 'react';
import styles from './styles.module.css'

export const Home = () => {

    const [page, setPage] = useState(1);

    const handelPageCount = (move) => {
        if(move === "next"){
            setPage(page+1);
        }else{
            if(page>1){
                setPage(page-1);
            }
        }
    }

    return(
        <div className="main_body">
            <div className={styles.gridContainer}>
                <div className={styles.mapContainer}>

                </div>
                <div className={styles.deviceListContainer}>
                    <div className={styles.pageinator}>
                        <div className={styles.prevPage}>
                            <span className="icon" style={{width:"100%",height:"100%"}} onClick={(event)=>handelPageCount("prev")}>
                                <i className="fa fa-chevron-circle-left fa-2x" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div className={styles.pageRange}>
                            {page}
                        </div>
                        <div className={styles.nextPage}>
                            <span className="icon" style={{width:"100%",height:"100%"}} onClick={(event)=>handelPageCount("next")}>
                                <i className="fa fa-chevron-circle-right fa-2x" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div className={styles.deviceList}>
                    </div>       
                </div>
            </div>
        </div>
    )
}