import React , {useState} from 'react';
import { Actions } from '../../actions';
import { useDispatch } from 'react-redux';
import { ajaxCall } from '../../utils/ajax';
import styles from './styles.module.css'

const urls = require("../../utils/urls.json");

export function Login() {

    const [user, setUser] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();

    
    const handleSubmit = (event) => {
        event.preventDefault();
        const reqObj = {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "username": user,
                "password": password
            })
        }
        const urlPath = urls.domain + urls.namespace + urls.endpoints.login;
        ajaxCall(
            urlPath,
            reqObj,
            (res) => {
                console.log(res)
                localStorage.setItem("token", res.token)
                dispatch(Actions.login())
            })
    }

    return (
        <div className="main_body">
            <div className={styles.loginFormContainer}>
                <form onSubmit={handleSubmit} method='POST'>
                    <p className="content has-text-centered title">
                        LOGIN
                    </p>
                    <div className="field">
                        <label className="label">Username</label>
                        <div className="control">
                        <input
                            className="input" 
                            type="text"
                            name="user"
                            value={user} 
                            placeholder="e.g test"
                            onChange={(e) => setUser(e.target.value)}
                            required
                            autoComplete="off"
                            />
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Password</label>
                        <div className="control">
                        <input
                            className="input"
                            type="password" 
                            placeholder="e.g. password"
                            name="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                            />
                        </div>
                    </div>
                    <div className="field is-grouped">
                        <div className="control">
                            <button id='lsubmit' className="button is-info" type="submit">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}