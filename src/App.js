import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Login, Home, Header } from './components';
import { ajaxCall } from './utils/ajax';
import 'bulma/css/bulma.css';
import './App.css';
import { Actions } from './actions';


const urls = require("./utils/urls.json");

const routePath = {
  root: "/",
  login: "/login",
  home: "/home"
}

const redirectToLoginPage = () => {
  return (
    <Redirect to={routePath.login} />
  )
}

const redirectToHomePage = () => {
  return (
    <Redirect to={routePath.home} />
  )
}

const checkTokenExist = (successCallback) => {
  console.log("called.....")
  const token = localStorage.getItem("token");
  if(token !== undefined || token !== ""){
    const reqObj = {
      method: "GET",
      headers: {
        "Authorization": "Bearer " + token
      }
    }
    const urlPath = urls.domain + urls.namespace + urls.endpoints.login;
    console.log(urlPath)
    ajaxCall(
      urlPath,
      reqObj,
      successCallback
    )
  }
}

const App = () => {
  
  const loginStatus = useSelector(state => state.loginStatus);
  const dispatch = useDispatch()

  checkTokenExist((res)=>{
    console.log(res);
    dispatch(Actions.login());
  })

  return (
      <Router>
          {/* <Loader /> */}
          <Header />
          <Switch>
            <Route
              path={routePath.root}
              exact
              component={redirectToLoginPage}
            />
            <Route
              path="/login"
              component={
                !loginStatus ? Login: redirectToHomePage
              }
            />
            <Route
              path="/home"
              component={
                  loginStatus ? Home : redirectToLoginPage
                }
            />
          </Switch>
      </Router>
  );
}

export default App;
